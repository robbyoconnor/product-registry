class ProductsSustainableDevelopmentGoal < ApplicationRecord
  belongs_to :product
  belongs_to :sustainable_development_goal
end