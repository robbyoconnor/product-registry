json.extract! workflow, :id, :name, :slug, :description,:created_at, :updated_at
json.url workflow_url(workflow, format: :json)
