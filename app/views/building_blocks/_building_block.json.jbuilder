json.extract! building_block, :id, :name, :slug, :created_at, :updated_at
json.url building_block_url(building_block, format: :json)
