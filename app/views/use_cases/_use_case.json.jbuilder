json.extract! use_case, :id, :name, :slug, :description, :sector_id, :created_at, :updated_at
json.url use_case_url(use_case, format: :json)
